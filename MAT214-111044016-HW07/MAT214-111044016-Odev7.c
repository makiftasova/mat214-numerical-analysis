/* 
 * File:   MAT214-111044016-Odev7.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 *
 * Created on May 29, 2013, 9:17 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static inline double func(const double x, const double y)
{
	return((y * pow(x, 2)) - y);
}

double euler(const int x_begin, const double y_begin, const double h, const int x_end)
{
	double y_old = y_begin;
	int index = x_begin;
	double y_new = 0.0;

	while (index < x_end) {

		y_new = y_begin + (func(index, y_old) * h);
		y_old = y_new;
		++index;
	}

	return y_old;

}

double heun(const int x_begin, const double y_begin, const double h, const int x_end)
{
	double y_old = y_begin;
	int index = x_begin;
	double y_new_zero = 0.0;
	double y_new = 0.0;

	while (index < x_end) {

		y_new_zero = y_old + (func(index, y_old) * h);

		y_new = (y_old + (((func(index, y_old) + func(index + 1, y_new_zero)) / 2.0) * h));

		y_old = y_new;
		++index;
	}

	return y_old;

}

/*
 *  Sadece 4. mertebeden Runge-Kutta çözümü yapar
 */
double runge_kutta(const int x_begin, const double y_begin, const double h, const int x_end)
{
	double y_old = y_begin;
	double index = x_begin;
	double y_new = 0.0;

	double k1 = 0.0, k2 = 0.0, k3 = 0.0, k4 = 0.0;

	while (index < x_end) {
		k1 = func(index, y_old);
		k2 = func(index + (0.5 * h), y_old + (0.5 * k1 * h));
		k3 = func(index + (0.5 * h), y_old + (0.5 * k2 * h));
		k4 = func(index + h, y_old + (k3 * h));

		y_new = (y_old + ((1.0 / 6.0) * ((k1 + (2 * k2) + (2 * k3) + k4) * h)));

		y_old = y_new;

		++index;
	}
	
	return y_old;

}

/*
 * 
 */
int main(int argc, char** argv)
{

	printf("Euler:\n------\n");
	printf("h = 1 => %.6lf\n", euler(0, 1.0, 1, 10));
	printf("h = 0.5 => %.6lf\n", euler(0, 1.0, 0.5, 10));
	printf("h = 0.1 => %.6lf\n", euler(0, 1.0, 0.1, 10));

	printf("\n");

	printf("Heun:\n-----\n");
	printf("h = 1 => %.6lf\n", heun(0, 1.0, 1, 10));
	printf("h = 0.5 => %.6lf\n", heun(0, 1.0, 0.5, 10));
	printf("h = 0.1 => %.6lf\n", heun(0, 1.0, 0.1, 10));
	
	printf("\n");
	
	printf("Runge-Kutta:\n------------\n");
	printf("h = 1 => %.6lf\n", runge_kutta(0, 1.0, 1, 10));
	printf("h = 0.5 => %.6lf\n", runge_kutta(0, 1.0, 0.5, 10));
	printf("h = 0.1 => %.6lf\n", runge_kutta(0, 1.0, 0.1, 10));

	

	return(EXIT_SUCCESS);
}

