/**
 * File:   main.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Mat214 - HW1
 * Calculates the root via bisection, false position,
 * secant, Newton-Raphson methods.
 *
 * Created on March 6, 2013, 10:06 PM
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * Lower bound of search interval
 */
#define LOWER_BOUND 1

/**
 * Upper bound of search interval
 */
#define UPPER_BOUND 10

/**
 * Initial test value for Newton-Raphson method
 */
#define INIT_NEW_RAP 7

/**
 * Epsilon value for calculations
 */
#define EPSILON 0.000000001

/**
 *  Maximum number of iterations for scant method 
 */
#define MAX_TRY 25

/**
 * An useful type for boolean expressions
 */
typedef enum bool_t {
	false = 0, true = 1
} boolean;

/**
 * Function to find root
 * 
 * @param year parameter of function
 * 
 * @return result of func(x)
 */
double func(double x);

/**
 * Derivative of function to find root
 * 
 * @param year parameter of function
 * 
 * @return result of derivFunc(x)
 */
double derivFunc(double x);

/**
 * x = g(x) form of Function for Iterative method
 * 
 * @param year parameter of function
 * 
 * @return result of iterFunc(x)
 */
double iterFunc(double x);

/**
 * Bisection method
 * 
 * @param f Function to find root
 * @param epsilon Maximum acceptable error
 * @param lowerBound Lower bound of interval
 * @param upperBound Upper bound of interval
 * @param rootFound if root found in given interval, this will be true, 
 * else false
 * 
 * @return Root of given function f which found first. If no root found returns 
 * 0.0 
 */
double bisection(double f(double), double epsilon, double upperBound,
		 double lowerBound, boolean* rootFound);

/**
 * 
 * False Position method
 * 
 * @param f Function to find root
 * @param epsilon Maximum acceptable error
 * @param lowerBound Lower bound of interval
 * @param upperBound Upper bound of interval
 * @param rootFound if root found in given interval, this will be true, 
 * else false
 * 
 * @return Root of given function f which found first. If no root found returns 
 * 0.0 
 */
double falsePosition(double f(double), double epsilon, double lowerBound,
		     double upperBound, boolean* rootFound);

/**
 * 
 * Secant method
 * 
 * @param f Function to find root
 * @param epsilon Maximum acceptable error
 * @param lowerBound Lower bound of interval
 * @param upperBound Upper bound of interval
 * @param rootFound if root found in given interval, this will be true, 
 * else false
 * 
 * @return Root of given function f which found first. If no root found returns 
 * 0.0 
 */
double secant(double f(double), double epsilon, double lowerBound,
	      double upperBound, boolean* rootFound);

/**
 * Newton-Raphson method
 * 
 * @param f Function to find root
 * @param derF Derivative of function
 * @param epsilon Maximum acceptable error
 * @param initTestVal Initial testing value
 * @param rootFound if root found in given interval, this will be true, 
 * else false
 * 
 * @return Root of given function f which found first. If no root found returns 
 * 0.0 
 */
double newtonRaphson(double f(double), double derF(double), double epsilon,
		     double initTestVal, boolean* rootFound);

/**
 * Iteration method
 * 
 * @param f Function to find root
 * @param epsilon Maximum acceptable error
 * @param lowerBound Lower bound of interval
 * @param upperBound Upper bound of interval
 * @param rootFound if root found in given interval, this will be true, 
 * else false
 * 
 * @return Root of given function f which found first. If no root found returns 
 * 0.0 
 */
double iteration(double f(double), double epsilon, double lowerBound,
		 double upperBound, boolean* rootFound);

/**
 * Calculates Xr of for False Position method than returns it
 * 
 * @param f Function to find root
 * @param lb Lower bound for function
 * @param ub Upper bound for function
 * 
 * @return Xr value for false position
 */
double fpMidFinder(double f(double), double lb, double ub);

/*
 * One function to rule them all
 */
int main(int argc, char** argv)
{

	boolean rootFound = false;
	double rootByBisection;
	double rootByFalsePosition;
	double rootBySecant;
	double rootByNewtonRaphson;
	double rootByIteration;


	rootByBisection =
		bisection(func, EPSILON, LOWER_BOUND, UPPER_BOUND, &rootFound);

	if (rootFound)
		printf("Root by Bisection: %.5f\n", rootByBisection);
	else
		printf("Bisection method cold not find a root\n");

	rootFound = false;

	rootByFalsePosition =
		falsePosition(func, EPSILON, LOWER_BOUND,
			UPPER_BOUND, &rootFound);

	if (rootFound)
		printf("Root by False Position: %.5f\n", rootByFalsePosition);
	else
		printf("False Position method cold not find a root\n");

	rootFound = false;

	rootBySecant =
		secant(func, EPSILON, 3, 5, &rootFound);

	if (rootFound)
		printf("Root by Secant: %.5f\n", rootBySecant);
	else
		printf("Secant method cold not find a root\n");

	rootFound = false;


	rootByNewtonRaphson =
		newtonRaphson(func, derivFunc, EPSILON, INIT_NEW_RAP, &rootFound);

	if (rootFound)
		printf("Root by Newton-Raphson: %.5f\n", rootByNewtonRaphson);
	else
		printf("Newton-Raphson method cold not find a root\n");

	rootFound = false;

	rootByIteration =
		iteration(func, EPSILON, LOWER_BOUND, UPPER_BOUND, &rootFound);

	if (rootFound)
		printf("Root by Iteration: %.5f\n", rootByIteration);
	else
		printf("Iteration method cold not find a root\n");

	return(EXIT_SUCCESS);
}

/* ### FUNCTIONS TO FIND ROOT ### */
double func(double x)
{
	return((-3000 * pow(1.2, x) + 175 * x) / (pow(1.2, x) - 1) + 5000);
}

double derivFunc(double x)
{
	double part1 = 0.0, part2 = 0.0, part3 = 0.0, result = 0.0;

	part1 = ((-1 * 31.9063) * pow(1.2, x) * x);
	part2 = ((721.965 * pow(1.2, x)) - 175);
	part3 = pow((pow(1.2, x) - 1), 2);

	return result = ((part1 + part2) / part3);
}

double iterFunc(double x)
{
	double part1 = 0.0, part2 = 0.0;
	
	part1 = log(((-175.0 * x) + 5000.0) / 2000.0);
	part2 = log(1.2);

	return(part1 / part2);
}

/* ### ROOT FINDER METHODS ### */
double bisection(double f(double), double epsilon, double lowerBound,
		 double upperBound, boolean* rootFound)
{
	double average = 0.0;
	*rootFound = false;

	if ((f(lowerBound) * f(upperBound)) < 0) {

		while (true) {
			average = ((upperBound + lowerBound) / 2.0);

			if ((f(lowerBound) * f(average)) < 0.0) {
				upperBound = average;
			} else {
				lowerBound = average;
			}


			if (fabs(f(average)) < epsilon) {
				*rootFound = true;
				break;
			}
		}
	}


	return average;
}

double falsePosition(double f(double), double epsilon, double lowerBound,
		     double upperBound, boolean* rootFound)
{
	double average = 0.0;
	*rootFound = false;

	if ((f(lowerBound) * f(upperBound)) < 0) {


		while (true) {
			average = fpMidFinder(f, lowerBound, upperBound);

			if ((f(lowerBound) * f(average)) < 0.0) {
				upperBound = average;
			} else {
				lowerBound = average;
			}


			if (fabs(f(average)) < epsilon) {
				*rootFound = true;
				break;
			}
		}
	}

	return average;
}

double secant(double f(double), double epsilon, double lowerBound,
	      double upperBound, boolean* rootFound)
{
	double root = 0.0, funcRoot = 0.0, funcL = 0.0, funcU = 0.0;

	/* for some space conservation */
	double lb = lowerBound, ub = upperBound;

	*rootFound = false;

	/* don't let the 0.0 remain as root */
	root = ((lb + ub) / 2.0);

	funcRoot = f(root);

	while (true) {

		funcL = f(lb);
		funcU = f(ub);

		root = lb + (ub - lb) * (-1 * funcL) / (funcU - funcL);
		lb = ub;
		ub = root;

		funcRoot = f(root);


		if (fabs(f(root)) < epsilon) {
			*rootFound = true;
			break;
		}
	}

	return root;
}

double iteration(double f(double), double epsilon, double lowerBound,
		 double upperBound, boolean* rootFound)
{
	double root = 0.0;

	root = ((lowerBound + upperBound) / 2.0);

	*rootFound = false;

	while (true) {

		if(fabs(f(root)) < epsilon) {
			*rootFound = true;
			break;
		}
		
		root = iterFunc(root);
	}

	return root;

}

double newtonRaphson(double f(double), double derF(double), double epsilon,
		     double initTestVal, boolean* rootFound)
{
	double average = 0.0;

	*rootFound = false;

	while (1) {
		average = (initTestVal - (f(initTestVal) / derF(initTestVal)));

		if (fabs(f(average)) < epsilon) {
			*rootFound = true;

			break;
		}

		initTestVal = average;
	}

	return average;
}

/* ### HELPERS FOR ROOT FINDER METHODS ### */
double fpMidFinder(double f(double), double lb, double ub)
{
	return(ub - ((f(ub) * (lb - ub)) / (f(lb) - f(ub))));
}
