/* 
 * File:   main.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Mat214 - HW02
 * Write a Program that can calculate determinant of a n*n Matrix
 *
 * Created on March 11, 2013, 8:25 PM
 * 
 * Determines a n*n Matrix Determinant
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "determinant.h"

/**
 * Size of matrix (value of n for matrix)
 */
#define MATRIX_SIZE 10

/* ## SOME USEFUL TYPE DEFINITIONS ## */

/*
 * One function to rule them all
 */
int main(int argc, char** argv)
{
	matrixPtr matrix = NULL;
	int i = 0, j = 0;

	/* initialize random number seed */
	srand(time(NULL));

	/* For run time calculations */
	clock_t t, start;
	start = clock();

	matrix = (matrixPtr) malloc(DBL_PTR_SIZ(MATRIX_SIZE));
	for (i = 0; i < MATRIX_SIZE; ++i)
		matrix[i] = (doublePtr) malloc(DBL_SIZ(MATRIX_SIZE));

	for (i = 0; i < MATRIX_SIZE; ++i)
		for (j = 0; j < MATRIX_SIZE; ++j)
			matrix[i][j] = (rand() % 20);

	printf("Matrix A =\n");

	for (i = 0; i < MATRIX_SIZE; ++i) {
		for (j = 0; j < MATRIX_SIZE; ++j) {
			printf("%6.3lf ", matrix[i][j]);
		}
		printf("\n");
	}

	printf("det(A) = %6.3lf\n", determinant(MATRIX_SIZE, matrix));


	free(matrix);

	/* Result of run time calculations */
	t = (clock() - start);

	printf("Time Taken=%f msec\n", (((double) t / CLOCKS_PER_SEC)*1000.00));

	return (EXIT_SUCCESS);
}

/* End of main.c */