/* 
 * File:   determinant.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * 
 * Mat214 - HW02
 * Write a Program that can calculate determinant of a n*n Matrix
 * 
 * Created on March 11, 2013, 8:25 PM
 * 
 * Header file for determinant calculations
 */

/* Check for multiple includes */
#ifndef DETERMINANT_H
#define	DETERMINANT_H

/* for C++ compatibility */
#ifdef	__cplusplus
extern "C" {
#endif /* end of C++ compatibility part */


	/**
	 *  Type definition for double pointer. It is actually a double*
	 */
	typedef double* doublePtr;

	/**
	 *  Type definition for Matrix pointers. It is actually a double**
	 */
	typedef double** matrixPtr;


	/* ## SOME USEFUL MACROS ## */

	/**
	 * double size of n units
	 */
#define DBL_SIZ(n) (sizeof(double) * (n))

	/**
	 * double* size of n units
	 */
#define DBL_PTR_SIZ(n) (sizeof(double*) * (n))

	/* ## Function Prototypes ## */

	/**
	 * Calculates Determinant of given n*n Matrix recursively 
	 * then returns it.
	 * 
	 * @param n Size of Matrix (must be in n*n form)
	 * @param m Matrix
	 * 
	 * @return Determinant of given Matrix
	 */
	double determinant(int n, matrixPtr matrix);


	
#ifdef	__cplusplus /* for C++ compatibility */
}
#endif
/* end of C++ compatibility part */

#endif	/* DETERMINANT_H */

/* End of determinant.h */