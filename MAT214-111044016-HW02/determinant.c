/* 
 * File:   determinant.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * 
 * Mat214 - HW02
 * Write a Program that can calculate determinant of a n*n Matrix
 * 
 * Created on March 11, 2013, 8:25 PM
 * 
 * Function definition for function determinant(int n, matrixPtr m)
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "determinant.h"

double determinant(int n, matrixPtr m)
{
	double det = 0.0, recPart = 0.0;

	matrixPtr tmp = NULL;

	size_t i = 0, j = 0, j1 = 0, j2 = 0;

	if (1 > n) {
		printf("[ERR] Matrix size is lower than 1 (n < 1)\n");
		fprintf(stderr, "[ERR] Matrix size is lower than 1 (n < 1)\n");
	} else if (1 == n) {
		det = (m[0][0]);
	} else if (2 == n) {
		det = ((m[0][0] * m[1][1]) - (m[1][0] * m[0][1]));
	} else {
		det = 0.0;

		for (j1 = 0; j1 < n; j1++) {

			/* allocate memory for operations */
			tmp = (matrixPtr) malloc(DBL_PTR_SIZ(n - 1));

			for (i = 0; i < (n - 1); ++i) {
				tmp[i] = (doublePtr) malloc(DBL_SIZ(n - 1));
			}

			/* build sub-matrix with minor elements excluded */
			for (i = 1; i < n; ++i) {
				j2 = 0; /* column index for sub-matrix */

				for (j = 0; j < n; ++j) {
					/*if j points selected element 
					 * to find minor 
					 */
					if (j == j1) {
						continue;
					}
					tmp[i - 1][j2] = m[i][j];

					++j2;
				}
			}

			/* Make recursive call.
			 * Use first line of real matrix for 
			 * calculating minors 
			 */
			recPart = determinant((n - 1), tmp);
			det += pow(-1.0, (double) j1) * m[0][j1] * recPart;



			/* Release previously allocated memory */
			for (i = 0; i < n - 1; i++)
				free(tmp[i]);
			free(tmp);
		}


	}
	return det;
}

/* End of determinant.c */