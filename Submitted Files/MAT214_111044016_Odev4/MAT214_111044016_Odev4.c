/**
 * File:   MAT214_111044016_Odev4.c
 * Author: Mehmet Akif TAŞOVA
 * Student Number: 111044016
 * 
 * MAT214 - HW04
 *
 * Created on March 28, 2013, 18:20 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/** 
 * Total Number of Known Equations
 */
#define EQUATION_NUM 10

/** 
 * Random number modifier
 */
#define RAND_MOD 10

/** 
 * Pointer for matrix
 */
typedef double** MATRIX;

/**
 *  Pointer for double
 */
typedef double* DOUBLEPTR;

/** 
 * Known x values of equations
 */
const int x[EQUATION_NUM] = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50};

/** 
 * Known y values of equations
 */
const int y[EQUATION_NUM] = {17, 25, 30, 33, 36, 38, 39, 40, 41, 42};


/**
 * Calculates General Terms for Linear Solution
 * 
 * @param m Matrix 
 * @param width width of matrix
 * @param hegiht height of matrix
 */
void calculateLinear(MATRIX m, const int width, const int hegiht);

/**
 * Calculates General Terms for Polynomial Solution
 * 
 * @param m Matrix 
 * @param width width of matrix
 * @param hegiht height of matrix
 *
 */
void calculatePolynomial(MATRIX m, const int width, const int hegiht);

/**
 * Prints given double value with appropriate sign
 * 
 * @param val
 */
void printSignedDouble(const double val);

/**
 * Prints equations to stdout
 * 
 * @param m Matrix of equations
 * @param width width of matrix
 * @param height height of matrix
 */
void printEquations(MATRIX m, const int width, const int height);

/**
 * Calculates gauss-jordan form of given matrix
 * 
 * @param m Matrix
 * @param width width of matrix
 * @param height height of matrix
 */
void gaussJordan(MATRIX m, const int width, const int height);

/**
 * Allocates memory for desired size of matrix
 * 
 * @param width width of matrix
 * @param height height of matrix
 * @return pointer to allocated area
 */
MATRIX allocMatrix(const int width, const int height);
/**
 * Makes free previously allocated matrix area
 * 
 * @param m Matrix
 * @param height height of matrix
 */
void freeMatrix(MATRIX m, const int height);

/**
 * Prints given matrix into stdout
 * 
 * @param m Matrix
 * @param width width of matrix 
 * @param height height of matrix
 */
void printMatrix(MATRIX m, const int width, const int height);
/**
 * Swaps two rows with each other. Never checks for Matrix size etc. 
 * (so be careful when using )
 * 
 * @param m MATRIX
 * @param row1 1st row
 * @param row2 2nd row
 */
void swapRows(MATRIX m, const int row1, const int row2);

/**
 * Swaps two columns with each other. Never checks for Matrix size etc. 
 * (so be careful when using )
 * 
 * @param m MATRIX
 * @param height Heigth of mtrix
 * @param col1 1st column
 * @param col2 2nd column
 */
void swapCols(MATRIX m, const int height, const int col1, const int col2);

/**
 * Converts given matrix into lower triangle matrix
 * 
 * @param m
 * @param width
 * @param height
 */
void lowerTriangleMatrix(MATRIX m, const int width, const int height);

/**
 * Converts given matrix into upper triangle matrix
 * 
 * @param m
 * @param width
 * @param height
 */
void upperTriangleMatrix(MATRIX m, const int width, const int height);

/*
 * 
 */
int main(int argc, char** argv)
{
	MATRIX matrix;
	int width = 3, height = 2;

	srand(time(NULL));

	matrix = allocMatrix(width, height);

	calculateLinear(matrix, width, height);
	printEquations(matrix, width, height);
	gaussJordan(matrix, width, height);

	puts("\nResult of Linear:\n-----------------\n");
	printf("f(x) = %.3fx + %.3f\n", matrix[0][2], matrix[1][2]);

	freeMatrix(matrix, height);

	puts("\n-----------------\n\n");


	++width;
	++height;
	matrix = allocMatrix(width, height);

	calculatePolynomial(matrix, width, height);
	printEquations(matrix, width, height);
	gaussJordan(matrix, width, height);

	puts("\nResult of Polynomial:\n---------------------\n");
	printf("f(x) = %.3f x^2 + %.3f x + %.3f", matrix[0][3],
		matrix[1][3], matrix[2][3]);

	freeMatrix(matrix, height);
	puts("\n---------------------\n\n");

	return(EXIT_SUCCESS);
}

void calculateLinear(MATRIX m, const int width, const int hegiht)
{
	double xSum = 0.0, ySum = 0.0, x2Sum = 0.0, xySum = 0.0;
	int i = 0;

	for (i = 0; i < EQUATION_NUM; ++i) {
		xSum += x[i];
		ySum += y[i];
		x2Sum += x[i] * x[i];
		xySum += x[i] * y[i];
	}

	m[0][0] = EQUATION_NUM;

	m[0][1] = xSum;
	m[1][0] = xSum;

	m[1][1] = x2Sum;

	m[0][2] = ySum;
	m[1][2] = xySum;
}

void calculatePolynomial(MATRIX m, const int width, const int hegiht)
{
	double xSum = 0.0, ySum = 0.0, x2Sum = 0.0, xySum = 0.0;
	double x2ySum = 0.0, x3Sum = 0.0, x4Sum = 0.0;
	int i = 0;

	for (i = 0; i < EQUATION_NUM; ++i) {
		xSum += x[i];
		ySum += y[i];
		x2Sum += x[i] * x[i];
		xySum += x[i] * y[i];
		x2ySum += x[i] * x[i] * y[i];
		x3Sum += x[i] * x[i] * x[i];
		x4Sum += x[i] * x[i] * x[i] * x[i];
	}

	m[0][0] = EQUATION_NUM;

	m[0][1] = xSum;
	m[1][0] = xSum;

	m[0][2] = x2Sum;
	m[1][1] = x2Sum;
	m[2][0] = x2Sum;

	m[1][2] = x3Sum;
	m[2][1] = x3Sum;

	m[2][2] = x4Sum;

	m[0][3] = ySum;

	m[1][3] = xySum;

	m[2][3] = x2ySum;
}

void printSignedDouble(const double val)
{
	printf(" ");
	if (val > 0.0)
		printf("+");
	printf("%.2f", val);
}

void printEquations(MATRIX m, const int width, const int height)
{
	int j, i;

	printf("Equations:\n----------\n" );
	printf("(x1 = x, x2 = y) \n");

	for (i = 0; i < height; ++i) {
		for (j = 0; j < width - 1; ++j) {
			printSignedDouble(m[j][i]);
			printf(" * x%d", j + 1);
		}
		printf(" = %.2f\n", m[i][width - 1]);
	}
}

void gaussJordan(MATRIX m, const int width, const int height)
{


	lowerTriangleMatrix(m, width, height);
	upperTriangleMatrix(m, width, height);

}

MATRIX allocMatrix(const int width, const int height)
{
	MATRIX m;
	int i;

	m = (MATRIX) (malloc(sizeof(DOUBLEPTR) * height));

	for (i = 0; i < height; ++i)
		m[i] = (DOUBLEPTR) malloc(sizeof(double) * width);

	return m;
}

void freeMatrix(MATRIX m, const int height)
{
	int i;
	for (i = 0; i < height; ++i)
		free(m[i]);
	free(m);
}

void printMatrix(MATRIX m, const int width, const int height)
{
	int i, j;
	for (i = 0; i < height; ++i) {
		for (j = 0; j < width; ++j)
			printf("%8.5f ", m[i][j]);
		printf("\n");
	}
}

void swapRows(MATRIX m, const int row1, const int row2)
{
	DOUBLEPTR temp;
	temp = m[row1];
	m[row1] = m[row2];
	m[row2] = temp;
}

void swapCols(MATRIX m, const int height, const int col1, const int col2)
{
	int row;
	double temp;
	for (row = 0; row < height; ++row) {
		temp = m[row][col1];
		m[row][col1] = m[row][col2];
		m[row][col2] = temp;
	}
}

void lowerTriangleMatrix(MATRIX m, const int width, const int height)
{
	int row, column, tempI, tempJ;
	float divider, subtrahend;

	for (row = 0, column = 0; row < height && column < width; ++row, ++column) {
		if (m[row][column] == 0)
			for (tempI = row; tempI < height; ++tempI)
				if (fabs(m[tempI][column]) > 0.01f)
					swapRows(m, row, tempI);


		if (fabs(1.0 - m[row][column]) > 0.01f) {
			divider = m[row][column];
			for (tempJ = 0; tempJ < width; ++tempJ)
				m[row][tempJ] /= divider;
		}

		for (tempI = row + 1; tempI < height; ++tempI) {
			subtrahend = m[tempI][column]; /* first value of the column */

			for (tempJ = 0; tempJ < width; ++tempJ)
				m[tempI][tempJ] -= subtrahend * (m[row][tempJ]);
		}
	}

	/*
	 * some values can be so close to zero,
	 * and some of them may be negative. For avoid
	 * displaying -0.00 values following lines will set
	 * all such values to zero
	 */
	for (tempI = 0; tempI < height; ++tempI)
		for (tempJ = 0; tempJ < width; ++tempJ)
			if (fabs(m[tempI][tempJ]) < 0.01f)
				m[tempI][tempJ] = 0;
}

void upperTriangleMatrix(MATRIX m, const int width, const int height)
{
	int row, column, tempI, tempJ;
	float divider, subtrahend;

	for (row = 0, column = 0; row < height && column < width; ++row, ++column) {
		if (m[row][column] == 0)
			for (tempJ = column; tempJ < width; ++tempJ)
				if (fabs(m[row][tempJ]) > 0.01f)
					swapCols(m, height, column, tempJ);


		if (fabs(1.0 - m[row][column]) > 0.01f) {
			divider = m[row][column];
			for (tempI = 0; tempI < height; ++tempI)
				m[tempI][column] /= divider;
		}

		for (tempJ = column + 1; tempJ < width - 1; ++tempJ) {
			subtrahend = m[row][tempJ]; /* first value of the column */

			for (tempI = 0; tempI < height; ++tempI)
				m[tempI][tempJ] -= subtrahend * (m[tempI][column]);
		}
	}

	/*
	 * some values can be so close to zero,
	 * and some of them may be negative. For avoid
	 * displaying -0.00 values following lines will set
	 * all such values to zero
	 */
	for (tempI = 0; tempI < height; ++tempI)
		for (tempJ = 0; tempJ < width; ++tempJ)
			if (fabs(m[tempI][tempJ]) < 0.01f)
				m[tempI][tempJ] = 0;
}

/* End of MAT214_111044016_Odev4.c */
