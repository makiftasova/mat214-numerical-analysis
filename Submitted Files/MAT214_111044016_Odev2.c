/* 
 * File:   MAT214_111044016_Odev2.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Mat214 - HW02
 *
 * Created on March 11, 2013, 7:27 PM
 * 
 * Determines a n*n Matrix Determinant
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/**
 * Size of matrix (value of n for matrix)
 */
#define MATRIX_SIZE 10

/* ## SOME USEFUL TYPE DEFINITIONS ## */

/**
 *  Type definition for double pointer. It is actually a double*
 */
typedef double* doublePtr;

/**
 *  Type definition for Matrix pointers. It is actually a double**
 */
typedef double** matrixPtr;


/* ## SOME USEFUL MACROS ## */

/**
 * double size of n units
 */
#define DBL_SIZ(n) (sizeof(double) * (n))

/**
 * double* size of n units
 */
#define DBL_PTR_SIZ(n) (sizeof(double*) * (n))

/* ## Function Prototypes ## */

/**
* Calculates Determinant of given n*n Matrix recursively 
* then returns it.
* 
* @param n Size of Matrix (must be in n*n form)
* @param m Matrix
* 
* @return Determinant of given Matrix
*/
double determinant(int n, matrixPtr matrix);

/*
 * One function to rule them all
 */
int main(int argc, char** argv)
{
	matrixPtr matrix = NULL;
	int i = 0, j = 0;

	/* initialize random number seed */
	srand(time(NULL));

	/* For run time calculations */
	clock_t t, start;
	start = clock();

	matrix = (matrixPtr) malloc(DBL_PTR_SIZ(MATRIX_SIZE));
	for (i = 0; i < MATRIX_SIZE; ++i)
		matrix[i] = (doublePtr) malloc(DBL_SIZ(MATRIX_SIZE));

	for (i = 0; i < MATRIX_SIZE; ++i)
		for (j = 0; j < MATRIX_SIZE; ++j)
			matrix[i][j] = (rand() % 20);

	printf("Matrix A =\n");

	for (i = 0; i < MATRIX_SIZE; ++i) {
		for (j = 0; j < MATRIX_SIZE; ++j) {
			printf("%6.3lf ", matrix[i][j]);
		}
		printf("\n");
	}

	printf("det(A) = %6.3lf\n", determinant(MATRIX_SIZE, matrix));


	free(matrix);

	/* Result of run time calculations */
	t = (clock() - start);

	printf("Time Taken=%f msec\n", (((double) t / CLOCKS_PER_SEC)*1000.00));

	return (EXIT_SUCCESS);
}

double determinant(int n, matrixPtr m)
{
	double det = 0.0;
	double recPart = 0.0; /* A temp stack for recursive part */

	matrixPtr tmp = NULL;

	size_t i = 0, j = 0, j1 = 0, j2 = 0;

	if (1 > n) {
		printf("[ERR] Matrix size is lower than 1 (n < 1)\n");
		fprintf(stderr, "[ERR] Matrix size is lower than 1 (n < 1)\n");
	} else if (1 == n) {
		det = (m[0][0]);
	} else if (2 == n) {
		det = ((m[0][0] * m[1][1]) - (m[1][0] * m[0][1]));
	} else {
		det = 0.0;

		for (j1 = 0; j1 < n; j1++) {

			/* allocate memory for operations */
			tmp = (matrixPtr) malloc(DBL_PTR_SIZ(n-1));

			for (i = 0; i < (n - 1); ++i) {
				tmp[i] = (doublePtr) malloc(DBL_SIZ(n-1));
			}

			/* build sub-matrix with minor elements excluded */
			/* ignore the first line of real matrix */
			for (i = 1; i < n; ++i) {
				j2 = 0; /* column index for sub-matrix */

				for (j = 0; j < n; ++j) {
					/*if j points selected element 
					 * to find minor 
					 */
					if (j == j1) {
						continue;
					}
					tmp[i - 1][j2] = m[i][j];

					++j2;
				}
			}

			/* Make recursive call.
			 * Use first line of real matrix for 
			 * calculating minors 
			 */
			recPart = determinant((n - 1), tmp);
			det += pow(-1.0, (double) j1) * m[0][j1] * recPart;



			/* Release previously allocated memory */
			for (i = 0; i < n - 1; i++)
				free(tmp[i]);
			free(tmp);
		}


	}
	return det;
}
