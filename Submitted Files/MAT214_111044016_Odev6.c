/* 
 * File:   main.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * MAT214 - HW06
 * 
 * Created on May 8, 2013, 3:54 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const double BEGIN = 0.0; /* Beginning of Integral Limit */
const double END = 1.0; /* End of Integral Limit */

static inline double func(double x)
{
	return pow(15.3, (2.5 * x));
}

inline double h(double begin, double end, double num)
{
	num = ((num < 1) ? 1 : num); /* Error check */
	return((end - begin) / num);
}

inline double multiSegmentTrapezoid(int numOfSegments)
{
	double result = 0.0;
	double intervalLen = h(BEGIN, END, (double) numOfSegments);
	int i = 0; /* loop counter */

	result = func(BEGIN);
	result += func(END);

	for (i = 1; i < numOfSegments; ++i) {
		result += 2 * func(BEGIN + (intervalLen * i));
	}

	result *= ((END - BEGIN) / (2.0 * numOfSegments));

	return result;
}

inline double multiSimpsonOneThree(int numOfSegments)
{
	double result = 0.0;
	double intervalLen = h(BEGIN, END, (double) numOfSegments);

	double tmpNum = 0.0;
	int i = 0; /* loop counter */

	result = func(BEGIN);
	result += func(END);

	for (i = 1; i < numOfSegments; ++i) {
		tmpNum = 2 * func(BEGIN + (intervalLen * i));
		result += ((i % 2) ? (tmpNum * 2.0) : tmpNum);
	}

	result *= ((END - BEGIN) / (3.0 * numOfSegments));

	return result;
}

inline double simpsonThreeEight(void)
{
	double result = 0.0;
	double param1 = (((2.0 * BEGIN) + END) / 3.0);
	double param2 = (((2.0 * END) + BEGIN) / 3.0);

	result = func(BEGIN);
	result += func(END);
	result += 3.0 * func(param1);
	result += 3.0 * func(param2);

	result *= ((END - BEGIN) / 8);

	return result;
}

inline double simpsonThreeEight2(void)
{
	double result = 0.0;
	double param1 = (((2.0 * BEGIN) + END) / 3.0);
	double param2 = (((2.0 * END) + BEGIN) / 3.0);

	result = func(BEGIN);
	result += func(END);
	result += (3.0 * func(param1));
	result += (3.0 * func(param2));

	result *= ((3.0 * (END - BEGIN)) / 8);

	return result;
}

inline double multiSimpsonThreeEight(int numOfSegments)
{
	double result = 0.0;
	double intervalLen = h(BEGIN, END, (double) numOfSegments);
	int i = 0; /* loop counter */
	double tmpNum = 0.0;

	result = func(BEGIN);
	result += func(END);

	for (i = 1; i < numOfSegments; ++i) {
		tmpNum = 2 * func(BEGIN + (intervalLen * i));
		result += ((i % 3) ? (tmpNum * 2.0) : (tmpNum * 3.0));
	}

	result *= ((3.0 * intervalLen) / 8);

	return result;
}

/*
 * One Function to rule them all
 */
int main(int argc, char** argv)
{
	puts("Trapezoid\n---------");
	printf("Trapezoid : %.6f\n", multiSegmentTrapezoid(1));

	puts("\n");

	puts("Multiple Segment Trapezoid\n--------------------------");
	printf("Trapezoid for n =  2: %.6f\n", multiSegmentTrapezoid(2));
	printf("Trapezoid for n =  4: %.6f\n", multiSegmentTrapezoid(4));
	printf("Trapezoid for n = 10: %.6f\n", multiSegmentTrapezoid(10));
	printf("Trapezoid for n = 20: %.6f\n", multiSegmentTrapezoid(20));

	puts("\n");

	puts("Simpson 1/3\n-----------");
	printf("Simpson 1/3: %.6f\n", multiSimpsonOneThree(1));

	puts("\n");

	puts("Multiple Segment Simpson 1/3\n----------------------------");
	printf("Simpson 1/3 for n =  4: %.6f\n", multiSimpsonOneThree(4));
	printf("Simpson 1/3 for n = 10: %.6f\n", multiSimpsonOneThree(10));
	printf("Simpson 1/3 for n = 20: %.6f\n", multiSimpsonOneThree(20));

	puts("\n");

	puts("Simpson 3/8\n-----------");
	printf("Simpson 3/8 : %.6f\n", simpsonThreeEight());

	puts("\n");

	puts("Simpson 3/8(Other Method)\n-------------------------");
	printf("Simpson 3/8 : %.6f\n", simpsonThreeEight2());

	puts("\n");

	puts("Multiple Segment Simpson 3/8\n----------------------------");
	printf("Simpson 3/8 for n = 1: %.6f\n", multiSimpsonThreeEight(1));
	return(EXIT_SUCCESS);
}