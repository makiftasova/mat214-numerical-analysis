/* 
 * File:   MAT214_111044016_Odev3_Part1.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * MAT214 - HW03 PART01
 * LU Decomposition
 *
 * Created on March 24, 2013, 11:45 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/** 
 * Size of Matrix
 */
#define MATRIX_SIZE 8

/** 
 * Modulo for Random Numbers. Identifies upper bound of random numbers.
 * Lower bound is always zero.
 */
#define RAND_MOD 13
/** 
 * End line character. Name starts with MAKIF because of there could be other
 * definitions named ENDL in standart libraries
 */
#define MAKIF_ENDL " \n" 

/* Some typedef for readability */
typedef double** MATRIX_PTR;
typedef double* DOUBLE_PTR;

/**
 * An useful type for boolean expressions
 */
typedef enum bool_t {
	false = 0, true = 1
} boolean;

/**
 * Takes Matrix then calculates its Lower and Upper triangle matrixes
 * 
 * @param m Original Matrix
 * @param size Size of matrix
 * @param l Lower triangle Matrix which calculated by function
 * @param u Upper triangle Matrix which calculated by function
 */
void luDecomposition(MATRIX_PTR m, const int size, MATRIX_PTR l, MATRIX_PTR u);

/**
 * Swaps two row of given matrix if possible
 * 
 * @param m Matrix
 * @param size Size of matrix for error check
 * @param row1 Index of first row to change position
 * @param row2 Index of second row to swap with first one
 * 
 * @return If success return true(1) else returns false(0)
 */
boolean swapRows(MATRIX_PTR m, const int size, const int row1, const int row2);

/**
 * Prints given matrix to given output
 * 
 * @param outputStream Where to print (stdout for terminal, or any text file 
 *						which opened with fopen before)
 * @param m Matrix to print
 * @param size Size of given matrix
 */
void printMatrix(FILE* outputStream, MATRIX_PTR m, const int size);

/*
 * 
 */
int main(int argc, char** argv)
{
	int i = 0, j = 0; /* loop counters */
	MATRIX_PTR matrix = NULL; /* matrix */
	MATRIX_PTR lower = NULL; /* lower triangle matrix */
	MATRIX_PTR upper = NULL; /* upper triangle matrix */

	/* initialize random number seed */
	srand(time(NULL));

	/* Allocate memory for matrixes */
	matrix = (MATRIX_PTR) malloc(sizeof (DOUBLE_PTR) * MATRIX_SIZE);
	lower = (MATRIX_PTR) malloc(sizeof (DOUBLE_PTR) * MATRIX_SIZE);
	upper = (MATRIX_PTR) malloc(sizeof (DOUBLE_PTR) * MATRIX_SIZE);

	for (i = 0; i < MATRIX_SIZE; ++i) {
		matrix[i] = (DOUBLE_PTR) malloc(sizeof (double) * MATRIX_SIZE);
		lower[i] = (DOUBLE_PTR) malloc(sizeof (double) * MATRIX_SIZE);
		upper[i] = (DOUBLE_PTR) malloc(sizeof (double) * MATRIX_SIZE);
	}

	/* fill original matrix with random numbers */
	for (i = 0; i < MATRIX_SIZE; ++i) {
		for (j = 0; j < MATRIX_SIZE; ++j) {
			matrix[i][j] = rand() % RAND_MOD;

			/* 
			 * also fill lower and upper triangle 
			 * matrix with zeros
			 */
			lower[i][j] = 0.0;
			upper[i][j] = 0.0;
		}
	}

	/* print original matrix to stdout */
	printf("original matrix:\n----------------\n");
	printMatrix(stdout, matrix, MATRIX_SIZE);
	printf(MAKIF_ENDL);

	/* begin calculations */
	luDecomposition(matrix, MATRIX_SIZE, lower, upper);

	/* print lower triangle matrix to stdout */
	printf("lower triangle:\n---------------\n");
	printMatrix(stdout, lower, MATRIX_SIZE);
	printf(MAKIF_ENDL);

	/* print upper triangle matrix to stdout */
	printf("upper triangle:\n---------------\n");
	printMatrix(stdout, upper, MATRIX_SIZE);
	printf(MAKIF_ENDL);

	/* free memory which allocated for matrixes */
	for (i = 0; i < MATRIX_SIZE; ++i) {
		free(matrix[i]);
		free(lower[i]);
		free(upper[i]);
	}

	free(matrix);
	free(lower);
	free(upper);

	/* End of life */
	return (EXIT_SUCCESS);
}

void luDecomposition(MATRIX_PTR m, const int size, MATRIX_PTR l, MATRIX_PTR u)
{
	int r = 0; /* row counter */
	int c = 0; /* column counters */
	int i = 0, j = 0; /* general use loop counters */
	int newRow = -1; /* index of new row, for swapping rows */
	double coeff = 0.0; /* coefficient for multiply a row with */

	/* Prepare lower and upper triangle matrixes for worst case */
	for (r = 0; r < size; ++r) {
		for (c = 0; c < size; ++c) {
			l[r][c] = 0.0;
			u[r][c] = 0.0;
		}
	}

	r = 0;
	c = 0;

	for (r = 0; r < size; ++r, ++c) {

		/* If current row starts with a zero */
		if (m[r][c] == 0.0) {
			newRow = -1;

			for (i = r + 1; i < size; ++i)
				if (m[i][c] != 0) {
					newRow = i;
					break;
				}

			/* swap rows if newRow found*/
			if (newRow != -1)
				swapRows(m, size, r, newRow);

		}

		/* Make zero below current column */
		for (i = r + 1; i < size; ++i) {
			coeff = (m[i][c] / m[r][c]);

			for (j = c; j < size; ++j)
				m[i][j] -= (m[r][j] * coeff);

			/* from definition of LU decomposition */
			m[i][c] = coeff;
		}

		/* Generate lower and upper triangle matrixes */
		for (i = 0; i < size; ++i) {
			/* lower triangle matrix main diagonal is always 1 */
			l[i][i] = 1.0;

			/*fill the rest of lower triangle matrix */
			for (j = (i + 1); j < size; ++j)
				l[j][i] = m[j][i];

			/* 
			 * upper triangle matrix main diagonal 
			 * is same with original matrix
			 */
			u[i][i] = m[i][i];
			/*fill the rest of upper triangle matrix */
			for (j = (i - 1); j >= 0; --j)
				u[j][i] = m[j][i];

		}
	}

	/* Just in memoriam of return */
	return;
}

boolean swapRows(MATRIX_PTR m, const int size, const int row1, const int row2)
{
	DOUBLE_PTR tmp;

	/* at least one of rows out of matrix */
	if ((row1 >= size) || (row2 >= size))
		return false;
	/* there is no negative index at rows */
	if ((row1 < 0) || (row2 < 0))
		return false;

	/* swap rows*/
	tmp = m[row1];
	m[row1] = m[row2];
	m[row2] = tmp;

	return true;
}

void printMatrix(FILE* outputStream, MATRIX_PTR m, const int size)
{
	int i = 0, j = 0; /* generic and most loved loop counters */

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			if (0.0 > m[i][j])
				printf("%-1c", ' ');
			else
				printf("%-2c", ' ');

			fprintf(outputStream, "%6.2f", m[i][j]);
		}
		/* end of row */
		fprintf(outputStream, "\n");
	}

	return;
}

/* End of MAT214_111044016_Odev3_Part1.c */
